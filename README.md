# TKobo

Utilitaire pour modifier rapidement les informations sur les ebooks contenus dans une liseuse Kobo.

Permet en particulier de définir le nom de la série à laquelle appartient un livre et le numéro dans cette série.

Testé uniquement sur le modèle Kobo Mini.
