/*
Copyright (C) 2013-2022 Teddy Michel

This file is part of TKobo.

TKobo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TKobo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TKobo. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CDIALOGEDITBOOK_HPP
#define T_FILE_CDIALOGEDITBOOK_HPP

#include <QDialog>
#include <QSqlDatabase>


class QLineEdit;
class QSpinBox;


class CDialogEditBook : public QDialog
{
    Q_OBJECT

public:

    CDialogEditBook(int bookId, QSqlDatabase dataBase, QWidget * parent = nullptr);

signals:

    void onBookChange(int bookId, const QString& title, const QString& author, const QString& series, int seriesNumber, const QString& subtitle);

protected slots:

    void save();
    void applyChanges();

private:

    int m_bookId;
    QSqlDatabase m_dataBase;
    QLineEdit * m_editTitle;
    QLineEdit * m_editAuthor;
    QLineEdit * m_editSeries;
    QSpinBox * m_editNumber;
    QLineEdit* m_editSubtitle;
};

#endif // T_FILE_CDIALOGEDITBOOK_HPP
