/*
Copyright (C) 2013-2022 Teddy Michel

This file is part of TKobo.

TKobo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TKobo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TKobo. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CMainWindow.hpp"
#include "CDialogEditBook.hpp"

#include <QTableView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStandardItemModel>
#include <QHeaderView>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QDir>


CMainWindow::CMainWindow() :
QMainWindow  (),
m_table      (nullptr),
m_model      (nullptr),
m_editDrive  (nullptr),
m_buttonLoad (nullptr),
m_buttonEdit (nullptr)
{
    setMinimumSize(400, 300);
    resize(800, 600);

    QVBoxLayout * layout = new QVBoxLayout();
    QWidget * widget = new QWidget(this);
    setCentralWidget(widget);
    widget->setLayout(layout);
    
    // Choix du lecteur
    QHBoxLayout * layoutTop = new QHBoxLayout();
    layout->addLayout(layoutTop);

    QLabel * labelDrive = new QLabel(tr("Drive:"));
    layoutTop->addWidget(labelDrive);

    m_editDrive = new QComboBox();
    m_editDrive->setMinimumWidth(100);
    m_editDrive->setEditable(true);
    layoutTop->addWidget(m_editDrive);

    // Liste des lecteurs
    QFileInfoList drives = QDir::drives();

    foreach (QFileInfo drive, drives)
    {
        m_editDrive->addItem(drive.filePath());
    }

    m_buttonLoad = new QPushButton(tr("Load"));
    layoutTop->addWidget(m_buttonLoad);
    layoutTop->addStretch();

    // Tableau
    m_table = new QTableView();
    layout->addWidget(m_table);
    m_table->setAlternatingRowColors(true);
    m_table->setSortingEnabled(true);
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_table->setSelectionMode(QAbstractItemView::SingleSelection);
    m_table->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    m_table->verticalHeader()->hide();
    m_table->verticalHeader()->setDefaultSectionSize(20);
    m_table->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    
    m_model = new QStandardItemModel(this);
    m_table->setModel(m_model);
    m_model->setSortRole(Qt::UserRole + 2);
    
    // Bouton "Edit"
    QHBoxLayout * layoutBottom = new QHBoxLayout();
    layout->addLayout(layoutBottom);

    m_buttonEdit = new QPushButton(tr("Edit selected book..."));
    m_buttonEdit->setEnabled(false);
    layoutBottom->addWidget(m_buttonEdit);
    layoutBottom->addStretch();

    QPushButton * buttonClose = new QPushButton(tr("Close"));
    layoutBottom->addWidget(buttonClose);
    
    connect(buttonClose, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_buttonLoad, SIGNAL(clicked()), this, SLOT(loadDataBase()));
    connect(m_buttonEdit, SIGNAL(clicked()), this, SLOT(editBook()));
    connect(m_table->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)), this, SLOT(onSelectedBookChange(QModelIndex, QModelIndex)));

    loadDataBase(false);
    m_table->resizeColumnsToContents();
}


CMainWindow::~CMainWindow()
{
    m_dataBase.close();
}


void CMainWindow::loadDataBase()
{
    loadDataBase(true);
}


void CMainWindow::loadDataBase(bool warning)
{
    QString fileName = m_editDrive->currentText() + ".kobo/KoboReader.sqlite";

    m_model->clear();
    m_model->setHorizontalHeaderLabels(QStringList() << tr("Title") << tr("Author") << tr("Series") << tr("Number") << tr("Filename") << tr("Subtitle"));

    // Chargement de la base de données
    m_dataBase = QSqlDatabase::addDatabase("QSQLITE", "KoboReader");
    m_dataBase.setDatabaseName(fileName);
    m_dataBase.setUserName("root");
    m_dataBase.setPassword("");
    
    if (!m_dataBase.open())
    {
        if (warning)
        {
            QMessageBox::warning(this, QString(), tr("Can't open database."));
        }

        return;
    }

    QSqlQuery query(m_dataBase);
    
    // Liste des livres
    if (!query.exec("SELECT rowid, Title, Attribution, Series, SeriesNumber, ContentID, SubTitle FROM content WHERE ContentType == 6 AND IsDownloaded == 'true'"))
    {
        QMessageBox::warning(this, QString(), tr("Can't load books from database: %1").arg(query.lastError().text()));
        return;
    }
    else
    {
        while (query.next())
        {
            QList<QStandardItem *> items;

            QStandardItem * itemTitle = new QStandardItem(query.value(1).toString());
            itemTitle->setData(query.value(0), Qt::UserRole + 1);
            itemTitle->setData(query.value(1).toString(), Qt::UserRole + 2);
            items.append(itemTitle);

            QStandardItem * itemAuthor = new QStandardItem(query.value(2).toString());
            itemAuthor->setData(query.value(0), Qt::UserRole + 1);
            itemAuthor->setData(query.value(2).toString(), Qt::UserRole + 2);
            items.append(itemAuthor);

            QStandardItem * itemSeries = new QStandardItem(query.value(3).toString());
            itemSeries->setData(query.value(0), Qt::UserRole + 1);
            itemSeries->setData(query.value(3).toString(), Qt::UserRole + 2);
            items.append(itemSeries);

            QStandardItem * itemNumber = new QStandardItem(query.value(4).toString());
            itemNumber->setData(query.value(0), Qt::UserRole + 1);
            itemNumber->setData(query.value(4).toInt(), Qt::UserRole + 2);
            items.append(itemNumber);

            QStandardItem * itemFilename = new QStandardItem(query.value(5).toString());
            itemFilename->setData(query.value(0), Qt::UserRole + 1);
            itemFilename->setData(query.value(5).toString(), Qt::UserRole + 2);
            items.append(itemFilename);

            QStandardItem * itemSubtitle = new QStandardItem(query.value(6).toString());
            itemSubtitle->setData(query.value(0), Qt::UserRole + 1);
            itemSubtitle->setData(query.value(6).toString(), Qt::UserRole + 2);
            items.append(itemSubtitle);

            m_model->appendRow(items);
        }
    }
}


void CMainWindow::onSelectedBookChange(const QModelIndex& current, const QModelIndex& previous)
{
    Q_UNUSED(previous);
    m_buttonEdit->setEnabled(current.isValid());
}


void CMainWindow::editBook()
{
    QStandardItem * item = m_model->itemFromIndex(m_table->currentIndex());

    if (item != nullptr)
    {
        int bookId = item->data(Qt::UserRole + 1).toInt();

        CDialogEditBook * dialog = new CDialogEditBook(bookId, m_dataBase, this);
        connect(dialog, SIGNAL(onBookChange(int, QString, QString, QString, int, QString)), this, SLOT(onBookChange(int, QString, QString, QString, int, QString)));
        dialog->show();
    }
}


void CMainWindow::onBookChange(int bookId, const QString& title, const QString& author, const QString& series, int seriesNumber, const QString& subtitle)
{
    for (int row = 0; row < m_model->rowCount(); ++row)
    {
        QStandardItem * itemTitle = m_model->item(row, 0);
        Q_CHECK_PTR(itemTitle);

        if (itemTitle->data(Qt::UserRole + 1).toInt() == bookId)
        {
            QStandardItem * itemAuthor = m_model->item(row, 1);
            QStandardItem * itemSeries = m_model->item(row, 2);
            QStandardItem * itemNumber = m_model->item(row, 3);
            QStandardItem * itemSubtitle = m_model->item(row, 5);

            Q_CHECK_PTR(itemAuthor);
            Q_CHECK_PTR(itemSeries);
            Q_CHECK_PTR(itemNumber);
            Q_CHECK_PTR(itemSubtitle);

            itemTitle->setText(title);
            itemTitle->setData(title, Qt::UserRole + 2);

            itemAuthor->setText(author);
            itemAuthor->setData(author, Qt::UserRole + 2);

            itemSeries->setText(series);
            itemSeries->setData(series, Qt::UserRole + 2);

            itemSubtitle->setText(subtitle);
            itemSubtitle->setData(subtitle, Qt::UserRole + 2);

            if (seriesNumber == 0)
            {
                itemNumber->setText(QString());
            }
            else
            {
                itemNumber->setText(QString::number(seriesNumber));
            }

            itemNumber->setData(seriesNumber, Qt::UserRole + 2);
        }
    }
}
