/*
Copyright (C) 2013-2022 Teddy Michel

This file is part of TKobo.

TKobo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TKobo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TKobo. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QTextCodec>
#include <QPixmap>
#include <ctime>

#include "CMainWindow.hpp"


int main(int argc, char * argv[])
{
    srand(time(nullptr));

    QApplication app(argc, argv);
    app.setWindowIcon(QPixmap(":/icons/TKobo"));

    QCoreApplication::setOrganizationName("Ted");
    QCoreApplication::setApplicationName("TKobo");

    QTextCodec * codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);

    CMainWindow window;
    window.show();

    return app.exec();
}
