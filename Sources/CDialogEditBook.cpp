/*
Copyright (C) 2013-2022 Teddy Michel

This file is part of TKobo.

TKobo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TKobo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TKobo. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CDialogEditBook.hpp"

#include <QVBoxLayout>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>


CDialogEditBook::CDialogEditBook(int bookId, QSqlDatabase dataBase, QWidget * parent) :
QDialog      (parent),
m_bookId     (bookId),
m_dataBase   (dataBase),
m_editTitle  (nullptr),
m_editAuthor (nullptr),
m_editSeries (nullptr),
m_editNumber (nullptr)
{
    resize(400, 100);
    setModal(true);

    QVBoxLayout * layout = new QVBoxLayout();
    setLayout(layout);

    QFormLayout * formLayout = new QFormLayout();
    layout->addLayout(formLayout);

    // Formulaire
    m_editTitle = new QLineEdit();
    formLayout->addRow(tr("Title:"), m_editTitle);

    m_editAuthor = new QLineEdit();
    formLayout->addRow(tr("Author:"), m_editAuthor);

    m_editSeries = new QLineEdit();
    formLayout->addRow(tr("Series:"), m_editSeries);

    m_editNumber = new QSpinBox(0);
    m_editNumber->setRange(0, 999);
    formLayout->addRow(tr("Number:"), m_editNumber);

    m_editSubtitle = new QLineEdit();
    formLayout->addRow(tr("Subtitle:"), m_editSubtitle);

    // Boutons de la boite de dialogue
    QDialogButtonBox * buttonBox = new QDialogButtonBox();
    layout->addWidget(buttonBox);

    QPushButton * buttonSave = buttonBox->addButton(tr("OK"), QDialogButtonBox::AcceptRole);
    connect(buttonSave, SIGNAL(clicked()), this, SLOT(save()));

    QPushButton * buttonClose = buttonBox->addButton(tr("Cancel"), QDialogButtonBox::RejectRole);
    connect(buttonClose, SIGNAL(clicked()), this, SLOT(close()));

    QSqlQuery query(m_dataBase);
    query.prepare("SELECT Title, Attribution, Series, SeriesNumber, SubTitle FROM content WHERE rowid == ?");
    query.bindValue(0, m_bookId);
    
    // Liste des livres
    if (!query.exec())
    {
        QMessageBox::warning(this, QString(), tr("Can't load book informations from database: %1").arg(query.lastError().text()));
        close();
        return;
    }

    if (query.next())
    {
        m_editTitle->setText(query.value(0).toString());
        m_editAuthor->setText(query.value(1).toString());
        m_editSeries->setText(query.value(2).toString());
        m_editNumber->setValue(query.value(3).toInt());
        m_editSubtitle->setText(query.value(4).toString());
    }
}


void CDialogEditBook::save()
{
    applyChanges();
    close();
}


void CDialogEditBook::applyChanges()
{
    // Update title and author
    QString title = m_editTitle->text();
    QString author = m_editAuthor->text();

    if (title.isEmpty() || author.isEmpty())
    {
        QMessageBox::warning(this, QString(), tr("You need to define a title and an author for the book."));
        return;
    }

    QSqlQuery query(m_dataBase);
    query.prepare("UPDATE content SET Title = ?, Attribution = ?, Series = NULL, SeriesNumber = NULL, SubTitle = NULL WHERE rowid == ?");

    query.bindValue(0, title);
    query.bindValue(1, author);
    query.bindValue(2, m_bookId);

    if (!query.exec())
    {
        QMessageBox::warning(this, QString(), tr("Can't edit book informations: %1").arg(query.lastError().text()));
        return;
    }

    // Update series
    QString series = m_editSeries->text();
    int seriesNumber = m_editNumber->value();
    if (!series.isEmpty())
    {
        QSqlQuery query(m_dataBase);
        query.prepare("UPDATE content SET Series = ?, SeriesNumber = ? WHERE rowid == ?");

        query.bindValue(0, series);
        query.bindValue(1, (seriesNumber > 0 ? seriesNumber : QVariant()));
        query.bindValue(2, m_bookId);

        if (!query.exec())
        {
            QMessageBox::warning(this, QString(), tr("Can't edit book informations: %1").arg(query.lastError().text()));
            return;
        }
    }

    QString subtitle = m_editSubtitle->text();
    if (!subtitle.isEmpty())
    {
        QSqlQuery query(m_dataBase);
        query.prepare("UPDATE content SET SubTitle = ? WHERE rowid == ?");

        query.bindValue(0, subtitle);
        query.bindValue(1, m_bookId);

        if (!query.exec())
        {
            QMessageBox::warning(this, QString(), tr("Can't edit book informations: %1").arg(query.lastError().text()));
            return;
        }
    }

    emit onBookChange(m_bookId, title, author, series, seriesNumber, subtitle);
}
