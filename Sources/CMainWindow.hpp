/*
Copyright (C) 2013-2022 Teddy Michel

This file is part of TKobo.

TKobo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TKobo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TKobo. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef T_FILE_CMAINWINDOW_HPP
#define T_FILE_CMAINWINDOW_HPP

#include <QMainWindow>
#include <QSqlDatabase>


class QTableView;
class QStandardItemModel;
class QComboBox;
class QPushButton;
class QModelIndex;


class CMainWindow : public QMainWindow
{
    Q_OBJECT

public:

    CMainWindow();
    ~CMainWindow();

protected slots:

    void onSelectedBookChange(const QModelIndex& current, const QModelIndex& previous);
    void loadDataBase();
    void editBook();
    void onBookChange(int bookId, const QString& title, const QString& author, const QString& series, int seriesNumber, const QString& subtitle);

private:

    void loadDataBase(bool warning);

    QTableView * m_table;
    QStandardItemModel * m_model;
    QComboBox * m_editDrive;
    QPushButton * m_buttonLoad;
    QPushButton * m_buttonEdit;
    QSqlDatabase m_dataBase;
};

#endif // T_FILE_CMAINWINDOW_HPP
